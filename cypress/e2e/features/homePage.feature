Feature: Home page feature- This would cover scenarios pertaining to home page																									
																									
@smoke @Regression @FEP-123
Scenario: verify that user can navigate to the home  page and can see the list of elements loading	

Given A user is already logged in																									
When user is in Home page																									
Then user can see list of options of elements to be tested	

@smoke @FEP-124
Scenario: Verify AB Testing

Given user is in Home page 
When user clicks on AB Testing option
Then user should be navigated to AB Testing Page
And user can go Back

@smoke @FEP-125
Scenario: Verify Add Remove element

Given user is in Home page
When user clicks on Add Remove element option
Then user should be able to see Fork me on GitHub option on the Add Remove element page & Powered by Elemental Selenium
