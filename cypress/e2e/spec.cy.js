describe("sampleTestSenario", function () {
  it("testCasename", function () {
    cy.visit("http://the-internet.herokuapp.com/");
    cy.get("a[href='/checkboxes']").click();
    cy.get("#checkboxes > input[type=checkbox]:nth-child(3)").click();
    cy.get("#checkboxes > input[type=checkbox]:nth-child(1)").click();
    cy.get("#checkboxes > input[type=checkbox]:nth-child(3)").click();
    cy.go("back");
    cy.get("a[href='/dropdown']").click();
    cy.get(".example #dropdown").select("Option 2");
    cy.get(".example #dropdown").select("Option 1");
  });
});
