class homePage {
  element = {
    title: () => cy.get("#content > h1"),
    header: () => cy.get("#content > h2"),
  };
  checkIfThisTextIsPresent(title) {
    this.element.title().should("be.visible").should("have.text", title);
  }
  checkIfThisTextIsPresentInTheHeader(header) {
    this.element.header().should("be.visible").should("have.text", header);
  }
}

module.exports = new homePage();
