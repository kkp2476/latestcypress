import {
  Given,
  When,
  Then,
  And,
} from "@badeball/cypress-cucumber-preprocessor";
const homePagePO = require("../page_objects/homePagePO");

Given("A user is already logged in", () => {
  cy.visit("");
});

When("user is in Home page", () => {
  // cy.get("#content > h1")
  //   .should("be.visible")
  //   .should("have.text", "Welcome to the-internet");

  homePagePO.checkIfThisTextIsPresent("Welcome to the-internet");
  homePagePO.checkIfThisTextIsPresentInTheHeader("Available Examples");
});

Then("user can see list of options of elements to be tested", () => {
  cy.get("#content").find("li").should("not.have.length", 43);
  cy.get("#content").find("li").should("have.length", 44);
  cy.get("#content").find("li").should("not.have.length", 45);
});

When("user clicks on AB Testing option", () => {
  cy.get("#content > ul > li:nth-child(1) > a").click();
});
Then("user should be navigated to AB Testing Page", () => {
  // cy.get("#content > div > h3")
  //   .should("be.visible")
  //   .should("have.text", "A/B Test Control");
  cy.get("#content > div > p")
    .then(($el) => $el.text().trim())
    .should(
      "eq",
      "Also known as split testing. This is a way in which businesses are able to simultaneously test and learn different versions of a page to see which text and/or functionality works best towards a desired outcome (e.g. a user action such as a click-through)."
    );
});

And("user can go Back", () => {
  cy.go("back");
});

When("user clicks on Add Remove element option", () => {
  cy.get("#content > ul > li:nth-child(2) > a").click();
});

Then(
  "user should be able to see Fork me on GitHub option on the Add Remove element page & Powered by Elemental Selenium",
  () => {
    cy.get("img[alt='Fork me on GitHub']").should("be.visible");

    // cy.get("#page-footer > div > div > a")
    //   .should("be.visible")
    //   .should("have.text", "Elemental Selenium");

    cy.contains("Elemental Selenium");
  }
);

// it("user should go back", () => {
//   cy.go("back");
// });
// it("user will click on Add/Remove element", () => {
//   cy.get("#content > ul > li:nth-child(2) > a").click();
// });
